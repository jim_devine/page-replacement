James Devine
jmd149@pitt.edu
CS 1550
Project 3

	I did not implement any exception handeling; therefore the program will crash with improper input.
	
	When running opt, there is about a 2 1/2 minute delay before any output is printed.
	
	For Opt:
	
		Proper input: python vsim.py -n <num_frames> -a opt swim.trace
		
	
	For Random:
	
		Proper input: python vsim.py -n <num_frames> -a rand swim.trace
	
	For NRU
	
		Proper input: python vsim.py -n <num_frames> -a nru -r <refresh_time> swim.trace
	
	For Aging:
	
		Proper input: python vsim.py -n <num_frames> -a aging -r <refresh_time> swim.trace