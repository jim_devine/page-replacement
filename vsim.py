
import sys
import random
import time

#globals
counter = 0
next = {}
num_frames = 0
page_table = {}			#will be a dictionary of lists ex. pagetable[page#] = [R, D]
trace = {}				#trace[file_line_number] = line		just so i dont have to keep opening the file
next_pos = {}				#tells me where the next time the page is referenced ex: next_pos[20] = 12
current_pages ={}			#only keeps track of if the pages are in the page table
age = {}
page_faults = 0
writes = 0
#end globals

def aging(page, r_w):
	#find the page with the smallest number in the aging dictionary
	global page_faults
	global writes
	min = -1
	swap = 0
	for p in age:				#for each page in the age dictionary
		num = '0'
		for bit in age[p]:		#build a string to represent the binary number
			num = num + str(bit)
		value = int(num, 2)
		if min == -1 or value < min:		#make sure we get the smallest value
			min = value
			swap = p
	
	if page_table[swap][1] == 1:
		print('page fault - evict dirty')	
		writes = writes + 1
	else:
		print('page fault - evict clean')	
	page_faults = page_faults + 1
		
	del page_table[swap]
	del age[swap]
	if r_w == 'R':								#set the R bit
		page_table[page] = [1, 0]
		age[page] = [1, 0, 0, 0, 0, 0, 0, 0]
	else:										#set the D bit
		page_table[page] = [0, 1]
		age[page] = [0, 0, 0, 0, 0, 0, 0, 0]
		
			
			
			

def nru(page, r_w):
	choice = 0
	swap = 0
	message = ' '
	global writes
	for p in page_table:		
	
		if page_table[p][0] == 0 and page_table[p][1] == 0:		#preferred swap
			swap = p
			break
		elif page_table[p][0] == 0 and page_table[p][1] == 1:	#2nd best
			swap = p
			choice = 1
		elif page_table[p][0] == 1 and page_table[p][1] == 0:	#3rd best
			if choice == 0 or choice > 2:
				swap = p
				choice = 2
		else:													#last choice
			if choice == 0:
				swap = p
				choice = 3
	
	
	if page_table[swap][1] == 1:
		print('page fault - evict dirty')
		writes = writes + 1
	else:
		print('page fault - evict clean')
	
	del page_table[swap]					#always delete from page_table before we add to it
	
	if r_w == 'R':
		page_table[page] = [1, 0]
	else:
		page_table[page] = [0, 1]
	


def opt(page, pos, r_w):
	global writes
	max = 0
	swap = 0
	for temp in page_table:				#goes through the page table
	
		if len(next[temp]) == 0:		#not referenced again
			swap = temp
			break
			
		check = next[temp][len(next[temp]) -1]		#looks at the next position for this page
	
		if check > max:								#is this pages next position greater than max?
			max = check
			swap = temp	
	
	if page_table[swap][1] == 1:
		print('page fault - evict dirty')
		writes = writes + 1
	else:
		print('page fault - evict clean')
	del page_table[swap]
	
	if r_w == 'R':
		page_table[page] = [1,0]
	else:
		page_table[page] = [0,1]



def rand(page, r_w):
	keys = list(page_table.keys())			#makes a list of the keys in the page table
	r = random.choice(keys)					#randomly chooses one of those keys
	global writes

	if page_table[r][1] == 1:
		print('page fault - evict dirty')
		writes = writes + 1
	else:
		print('page fault - evict clean')
	
	del page_table[r]
	if r_w == 'R':
		page_table[page] = [1,0]
	else:
		page_table[page] = [0,1]



def bin(s):
	return str(s) if s<=1 else bin(s>>1) + str(s&1)

#takes the hex value and returns the page number
def get_page(hex):
	decimal = int(hex, 16)	#go from hex to decimal
	binary = bin(decimal)	#get the binary representation of that decimal
	size  = len(str(binary))	#get the size of the binary number
	page_num = '0'	
	for bit in binary:			#I want the page number in the 20 left-most bits, ignore the rightmost 12
	
		page_num = page_num + str(bit)		#builds the a binary string of the leftmost 20
		size = size - 1
		if size == 12:
			break	
	page = int(page_num,2)
	
	return page
	
#main method here   ----------------------------------------------------------------------------------------------------
start = time.clock()

#handle command line arguements	
argv_length = len(sys.argv) - 1		#the tracefile will always be at the end of the argvs
num_frames = int(sys.argv[2])			#gets the number of frames from the user
alg = sys.argv[4]
fo = fo = open(sys.argv[argv_length], "r")		#open the trace file
x = 0
time_1 = time.clock()


if alg == 'opt':
	for line in fo:
		if line == ' ':
			continue		

		spl = line.split(" ")	#turns line into a list
		hex = spl[0]
		r_w = spl[1].strip()
		page = get_page(hex)	#get the page

		trace[x] = [page, spl[1]]	#copies the trace into the trace dictionary
		
		if page in next:
			next[page].insert(0, x)
		else:
			next[page] = [x]			
		x = x + 1
	fo.close()	
			
	for y in trace:

		page = trace[y][0]			#page
		r_w = trace[y][1]			# 'R' or 'W' bit
	
		if page in page_table:			#hit
			if r_w == 'R':
				page_table[page][0] = 1
			else:
				page_table[page][1] = 1
			
			print('hit')
	
		elif len(page_table) < num_frames:		#just need to add it
			
			if r_w == 'R':
				page_table[page] = [1,0]
			else:
				page_table[page] = [0,1]
			
			print('page fault - no eviction')
			page_faults = page_faults + 1
				
		
		else:								#need to swap
			opt(page, y, r_w)
			page_faults = page_faults + 1
			
		next[page].pop()					#remove the reference from the next dictionary				
	
elif alg == 'rand':
	for line in fo:
		if line == ' ':
			continue		

		spl = line.split(" ")	#turns line into a list
		hex = spl[0]
		page = get_page(hex)
		r_w = spl[1].strip()
	
		if page in page_table:			#no need to swap
			next_pos[page] = 0
			if r_w == 'R':
				page_table[page][0] = 1
			else:
				page_table[page][1] = 1
				
			print('hit')
	
		elif len(page_table) < num_frames:		#just need to add it
			if r_w == 'R':
				page_table[page] = [1,0]
			else:
				page_table[page] = [0,1]
				
			page_faults = page_faults + 1
						
			print('page fault - no eviction')
					
		else:								#need to swap
			rand(page, r_w)						#randomly swap 		
			page_faults = page_faults + 1
		x = x + 1
	fo.close()
	

elif alg == 'nru':
	refresh = int(sys.argv[6])
	for line in fo:
		if line == ' ':
			continue

		#if time.clock() - time_1 > 2:
		#	time_1 = time.clock()
			
		if x % refresh == 0:			#refresh
			for p in page_table:
				page_table[p][0] = 0			

		spl = line.split(" ")	#turns line into a list
		hex = spl[0]
		r_w = spl[1].strip()
		
		page = get_page(hex)
	
		if page in page_table:			#no need to swap
			if r_w == 'R':
				page_table[page][0] = 1				
			else:
				page_table[page][1] = 1	
			print('hit')
	
		elif len(page_table) < num_frames:		#just need to add it	
			if r_w == 'R':
				page_table[page] = [1, 0]
				
			else:
				page_table[page] = [0, 1]
				
			print('page fault - no eviction')
			page_faults = page_faults + 1
				
		else:								#need to swap	
			nru(page, r_w)
			page_faults = page_faults + 1	
			
		x = x + 1
	fo.close()	
	
elif alg == 'aging':	
	refresh = int(sys.argv[6])
	for line in fo:
		if line == ' ':
			continue
			
	#	if time.clock() - time_1 > 2:
			#time_1 = time.clock()
			
		if x % refresh == 0:			#refresh	
			for pag in page_table:	#add the reference bit				
				age[pag].insert(0, page_table[page][0])
				age[pag].pop()
												
			
			for p in page_table:		#set all reference bits to 0
				page_table[p][0] = 0	

		spl = line.split(" ")	#turns line into a list
		hex = spl[0]
		r_w = spl[1].strip()
		
		page = get_page(hex)
	
		if page in page_table:			#no need to swap
			if r_w == 'R':
				page_table[page][0] = 1				
			else:
				page_table[page][1] = 1	
			print('hit')
	
		elif len(page_table) < num_frames:		#just need to add it	
			if r_w == 'R':
				page_table[page] = [1, 0]
				age[page] = [1, 0, 0, 0, 0, 0, 0, 0]		
			else:
				page_table[page] = [0, 1]
				age[page] = [0, 0, 0, 0, 0, 0, 0, 0]
			
			print('page fault - no eviction')
			page_faults = page_faults + 1
				
		else:								#need to swap	
			aging(page, r_w)							
			
		x = x + 1
	fo.close()	
	
	
#end = time.clock() - start
#print(end)	
print('Number of frames: ' + str(num_frames))
print('Total memory accesses: ' + str(x))
print('Total page faults: ' + str(page_faults))
print('Total writes to disk: ' + str(writes))




	
	
	
	
	
